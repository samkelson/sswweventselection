
fitDir=${1}

[ ! -d ${fitDir} ] && echo "ERROR: Please point to an output folder of TRexFitter" && exit

date=$(date +%F)

cd ${fitDir}/..
fitDir=$(basename ${fitDir})

outFile=fitPres-${fitDir}-${date}.tex

: > ${outFile}

cat >> ${outFile} <<EOF
\\documentclass{beamer}

\\usepackage{siunitx}
\\sisetup{separate-uncertainty,table-format=6.3(6)}

\\begin{document}

\\begin{frame}

\\includegraphics[trim=0 0 200 30, clip, width=.55\\linewidth]{${fitDir}/NormFactors.pdf}
\\includegraphics[trim=200 200 0 0, clip, width=.35\\linewidth]{${fitDir}/CorrMatrix.pdf}

\\end{frame}

\\begin{frame}
\\includegraphics[width=.45\\linewidth]{${fitDir}/Plots/SR.pdf}
\\includegraphics[width=.45\\linewidth]{${fitDir}/Plots/SR_postFit.pdf}
\\end{frame}

%\\begin{frame}
%(sed -n '/\\begin{tabular}/,/\\end{tabular}/p' ${fitDir}/Tables/Yields.tex)
%(sed -n '/\\begin{tabular}/,/\\end{tabular}/p' ${fitDir}/Tables/Yields_postFit.tex)
%\\end{frame}

\\end{document}

EOF

pdflatex ${outFile}
rm ${outFile/.tex}.{aux,log,nav,out,snm,toc}

echo "Created ${PWD}/${outFile/.tex/.pdf}"
