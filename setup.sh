
# Note: this setup script is to be executed within the container
cd /DelphesAnalysis

export PYTHONPATH=/usr/lib64/python3.10/site-packages/

if [ ! -d venv ] ; then
  python3 -m venv venv && . ./venv/bin/activate && pip install -r requirements.txt
else
  . ./venv/bin/activate
fi
