#!/bin/bash

#cp samples_WithSystematics_100TeV/$3/$4 samples100TeV

#singularity exec --bind ${PWD}:/DelphesAnalysis --bind samples_WithSystematics_100TeV:/DelphesAnalysis/Samples_100Tev --bind samples_WithSystematics_50TeV_27TeV:/DelphesAnalysis/Samples_50TeV_27TeV docker://rootproject/root bash /DelphesAnalysis/ana/runanaWhenUsingSingularity.sh "$1" "$2" "$3" "$4" "$5"

. /cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc8-opt/setup.sh

python3 ./ana/ana.py "$1" "$2/$3/$4" "$5"

#python3 ./ana/readingTest.py "$1" "$2/$3/$4" "$5"
