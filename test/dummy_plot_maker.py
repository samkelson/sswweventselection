#!/usr/bin/env python3

import ROOT
import numpy as np

colorMap = { "ssWWjjEWLL": ROOT.kBlue, "ssWWjjEWLT": ROOT.kRed, "ssWWjjEWTT": ROOT.kGreen, "ssWWjjEW": ROOT.kBlack }

ROOT.gStyle.SetOptStat(0)

rebin_array = np.array([0., 100., 200., 500., 1000.])

c = ROOT.TCanvas("Mll_MjjCut_all");
l = ROOT.TLegend()

f = ROOT.TFile.Open("output.root")

stack = ROOT.THStack("Mll_MjjCut_all","")
hists = { }
for s in [ "ssWWjjEWTT", "ssWWjjEWLT", "ssWWjjEWLL" ]:
  h = f.Get(f"Mll_MjjCut_{s}_all")
  h = h.Rebin(4, f"Mll_MjjCut_{s}_all", rebin_array)
  h.SetFillColor(colorMap[s])
  h.SetLineColor(colorMap[s])
  l.AddEntry(h, s, "F")
  hists[s] = h
  stack.Add(h)

stack.Draw("hist")

h = f.Get("Mll_MjjCut_ssWWjjEW_all")
h = h.Rebin(4, "Mll_MjjCut_ssWWjjEW_all", rebin_array)
h.SetLineColor(colorMap["ssWWjjEW"])
hists["ssWWjjEW"] = h
l.AddEntry(h, "ssWWjjEW", "L")


h.Draw("same")
l.Draw()

c.Print(".png")


# Making 'shape' plot

for s in [ "ssWWjjEWTT", "ssWWjjEWLT", "ssWWjjEWLL", "ssWWjjEW" ]:
  hists[s].Scale( 1. / hists[s].Integral() )

# Looking for the highest bin, to plot first
maxValue = 0
maxHistoSample = ""
for s in [ "ssWWjjEWTT", "ssWWjjEWLT", "ssWWjjEWLL", "ssWWjjEW" ]:
    hists[s].SetFillColor(0)
    maxHistValue = hists[s].GetMaximum()
    hists[s].Print("all")
    print(maxValue, maxHistValue)
    if maxValue < maxHistValue:
        maxValue = maxHistValue
        maxHistoSample = s

print(maxValue)
c = ROOT.TCanvas("Mll_MjjCut_all_shapePlot");
hists[maxHistoSample].Draw("hist")
for s in [ "ssWWjjEWTT", "ssWWjjEWLT", "ssWWjjEWLL", "ssWWjjEW" ]:
  if s != maxHistoSample: hists[s].Draw("same hist")

l.Draw()
c.Print(".png")

f.Close()
