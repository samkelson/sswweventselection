# DelphesAnalysis

## Setup Description

This repository contains various components:

- A Python-based RDataFrame analysis framework
- A C++-based repository of functionality using RDataFramw
- An API server to perform analysis as a service (AaaS), using the former


## Running within the environment

For development purposes, we recommend that you develop within the container, mounting your local folder within the container.

## Running in the container

#### Docker

```
docker run --rm -it -v "$PWD":/DelphesAnalysis rootproject/root /bin/bash
```

#### Singularity (e.g. on lxplus)

```
singularity run --bind ${PWD}:/DelphesAnalysis docker://rootproject/root -- /bin/bash
```

For Snowmass2021, you might want to also mount the `/collab` folder, like so:

```
singularity run --bind /collab:/collab --bind ${PWD}:/DelphesAnalysis docker://rootproject/root -- /bin/bash
```

We then recommend to develop within that environment for maximum compatiblity. You can still use your favourite editors as usual, as the work folder is shared.


### Adding more folders

Should you need to you, can always bind more local folders and make then available within the container.


## Running the code at BNL

To connect to BNL machines, follow instructions here: https://usatlas.readthedocs.io/projects/af-docs/en/latest/sshlogin/ssh2BNL/
Once connected, checkout this package to your user area (e.g /usatlas/groups/bnl_local2/yourusername) and run the code using singularity as follows

```
singularity run --bind ${PWD}:/DelphesAnalysis --bind samples_WithSystematics_100TeV:/DelphesAnalysis/Samples docker://rootproject/root -- /bin/bash
cd /DelphesAnalysis/
./ana/ana.py
```

where samples_WithSystematics_100TeV is the symbolic link to 100 TeV samples

For 50 TeV and 27 TeV samples, the symbolic link is samples_WithSystematics_50TeV_27TeV

Side note: Since it takes a couple of days to run over any full set of these samples, it is recommended to run in a screen session (see https://linuxize.com/post/how-to-use-linux-screen/) or similar. 

## Fitting

### Authentication to the CERN Gitlab Registry

In order to be able to access the image below, you need to log into the CERN GitLab registry:

```
singularity remote login -u ${GITLAB_USER} -p ${GITLAB_TOKEN} docker://gitlab-registry.cern.ch
```

where `GITLAB_USER` is the variable containing your CERN NICE login and `GITLAB_TOKEN` contains a GitLab  Personal Access Token with at least `read_registry` permissions. To set it up, go [here](https://gitlab.com/-/profile/personal_access_tokens).

### Running TRExFitter

One can use the provided `TRExFitter` configuration and run the fit as follows

```
docker run -it -v "$(pwd)":/DelphesAnalysis gitlab-registry.cern.ch/trexstats/trexfitter:latest
```

(note: to investigate with GUI, on macOS, one can run

```
docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$ip:0 -v "$(pwd)":/DelphesAnalysis gitlab-registry.cern.ch/trexstats/trexfitter:latest
```

but first needs to obtain and set `ip` and use `xhosts +$ip` to enable X11)

To run at BNL, use singularity as follows

```
singularity run --bind ${PWD}:/DelphesAnalysis docker://gitlab-registry.cern.ch/trexstats/trexfitter:latest
```

and then run TRexFitter

```
cd /DelphesAnalysis/fit
export PATH=/code/src/TRExFitter/build/bin:${PATH}
export LD_LIBRARY_PATH=/code/src/TRExFitter/build/lib:${LD_LIBRARY_PATH}
trex-fitter hwpdfsa TRexFitter/vbs_pol_fcc_hh.config 
```
